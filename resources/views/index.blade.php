<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel Test</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <script>
        jQuery(document).ready(function($) {
            jQuery('#product-form').on('submit', function(e) {
                e.preventDefault();
                jQuery.ajax({
                    type: "POST",
                    url: '/product/store',
                    data: $(this).serialize(),
                    success: function(result) {
                        jQuery("#product-tbody tr").remove();
                        jQuery("#product-tbody").append(result);
                    }
                });

            });
        });
    </script>
</head>

<body>
    <div class="container">
        <form id="product-form" action="{{url('/product/store')}}" method="POST">

            {{ csrf_field() }}

            <div class="form-group">
                <label for="product-name">Product name</label>
                <input type="text" class="form-control" id="product-name" name="product-name" placeholder="Enter product name here" required>
            </div>

            <div class="form-group">
                <label for="quantity">Quantity</label>
                <input type="text" class="form-control" id="quantity" name="quantity" placeholder="Enter quantity here" required>
            </div>

            <div class="form-group">
                <label for="price">Price</label>
                <input type="text" class="form-control" id="price" name="price" placeholder="Enter price here" required>
            </div>

            <div class="form-group">
                <input class="btn btn-lg btn-primary" id="submit" type="submit" value="Save" name="submit" />
            </div>

        </form>

        <h2>Product list</h2>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Product name</th>
                    <th>Quantity in stock</th>
                    <th>Price per item</th>
                    <th>Datetime submitted</th>
                    <th>Total value number</th>
                </tr>
            </thead>
            <tbody id="product-tbody">
                @include('partials/product')
            </tbody>
        </table>
    </div>
</body>

</html>