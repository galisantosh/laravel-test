<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\ProductStoreRequest;

class ProductController extends Controller
{
    public function index()
    {
        $productData = [];
        if(Storage::disk('local')->exists('data.json')) {
            $productData = Storage::disk('local')->get('data.json');
            $productData = json_decode($productData, true);
        }
        return view('index', ['products' => $productData]);
    }

    public function store(Request $request)
    {
        try {
            $productData = Storage::disk('local')->exists('data.json') ? json_decode(Storage::disk('local')->get('data.json')) : [];

            $inputData = $request->only(['product-name', 'quantity', 'price']);

            $inputData['created_at'] = date('Y-m-d H:i:s');

            array_push($productData, $inputData);

            Storage::disk('local')->put('data.json', json_encode($productData));
            
            
            $productData = Storage::disk('local')->get('data.json');
            $productData = json_decode($productData, true);
            return view('partials.product', ['products' => $productData]);
        } catch (Exception $e) {
            return ['error' => true, 'message' => $e->getMessage()];
        }
    }
}
